import { Container, Content, Header, Right, Row } from 'native-base';
import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { IHistoryItem } from '../Storage/IHistoryItem';
import { Storage } from '../Storage/Storage';

type state = {
  items: IHistoryItem[]
};

type props = {};

const styles = StyleSheet.create({
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: "bold"
  },
  content: {
    marginTop: 10,
    paddingHorizontal: 10
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 43,
    marginBottom: 10
  },
  buttonText: {
    fontSize: 18
  },
  row: {
    borderBottomColor: "#DDDDDD",
    borderBottomWidth: 1,
    paddingBottom: 4,
    marginBottom: 4,
    flexDirection: 'row-reverse',
    justifyContent: "space-between"
  },
  timestamp: {
    fontSize: 15,
    color: "#aaaaaa"
  },
  label: {
    fontSize: 19,
    fontWeight: "bold"
  }
});

export default class HistoryScreen extends React.Component<props, state> {

  constructor(props: props) {
    super(props);
    this.state = {
      items: []
    };
  }

  componentDidMount() {
    setInterval(async () => {
      this.setState({
        items: await Storage.readHistory()
      });
    }, 2000);

  }

  render() {
    return (
      <Container>
        <Header>
          <Right><Text style={styles.title}>تاریخچه</Text></Right>
        </Header>
        <Content style={styles.content}>
          {
            this.state.items.map((i, index) => (
              <Row key={index} style={styles.row}>
                <Text style={styles.label}>{i.label}</Text>
                <Text style={styles.timestamp}>{i.timestamp}</Text>
              </Row>
            ))
          }
        </Content>
      </Container >
    );
  }
}