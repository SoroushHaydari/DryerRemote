import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import * as SMS from 'expo-sms';
import { Ionicons } from "@expo/vector-icons";
import * as Font from 'expo-font';
import { Container, Header, Right, Content } from 'native-base';
import { Text } from "react-native";
import Buttons from "../Buttons.json";
import { AppLoading } from 'expo';
import { Storage } from '../Storage/Storage';

type states = {
  isReady: boolean;
};

type props = {};

const styles = StyleSheet.create({
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: "bold"
  },
  content: {
    marginTop: 10,
    paddingHorizontal: 10
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 43,
    marginBottom: 10
  },
  buttonText: {
    fontSize: 18
  }
});

export default class ButtonsScreen extends React.Component<props, states> {

  constructor(props: props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }

  render() {

    if (!this.state.isReady) {
      return <AppLoading />;
    }

    return (
      <Container>
        <Header>
          <Right><Text style={styles.title}>لیست دستورات</Text></Right>
        </Header>
        <Content style={styles.content}>
          {Object.entries(Buttons).map(
            pair =>
              <TouchableOpacity onPress={() => this.sendSms(pair[0], pair[1])} style={styles.button} key={pair[0]}>
                <Text style={styles.buttonText}>{pair[1]}</Text>
              </TouchableOpacity>
          )}
        </Content>
      </Container >
    );
  }

  async sendSms(code: string, label: string) {

    const phoneNumber = await Storage.readClientPhoneNumber();

    if (phoneNumber != null && phoneNumber.trim() !== "") {

      await Storage.addToHistory(label);

      await SMS.sendSMSAsync(
        [phoneNumber],
        code
      );
    }

  }

}