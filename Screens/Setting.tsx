import { Col, Container, Content, Header, Right, Toast } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, TextInput, Button } from 'react-native';
import { Storage } from '../Storage/Storage';

type states = {
  showDeveloperTools: boolean;
  clientPhoneNumber: string;
};

type props = {};

const styles = StyleSheet.create({
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: "bold"
  },
  content: {
    marginTop: 10,
    paddingHorizontal: 10
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 23,
    marginBottom: 10
  },
  buttonText: {
    fontSize: 18
  },
  input: { height: 40, borderColor: '#DDDDDD', borderWidth: 1 }
});

export default class SettingScreen extends React.Component<props, states> {

  constructor(props: props) {
    super(props);
    this.state = {
      showDeveloperTools: false,
      clientPhoneNumber: ""
    };
  }

  async componentDidMount(): Promise<void> {
    this.setState({ clientPhoneNumber: await Storage.readClientPhoneNumber() || "" });
  }

  render() {
    return (
      <Container>
        <Header>
          <Right><Text onLongPress={() => this.setState({ showDeveloperTools: !this.state.showDeveloperTools })} style={styles.title}>تنظیمات</Text></Right>
        </Header>
        <Content style={styles.content}>

          <TouchableOpacity style={styles.button} onPress={async () => {
            await Storage.clearHistory();

            Toast.show({
              text: 'تاریخچه پاک شد',
              duration: 2000
            });
          }}>
            <Text style={styles.buttonText}>پاک کردن تاریخچه</Text>
          </TouchableOpacity>

          {
            this.state.showDeveloperTools ? (
              <Col>
                <TextInput style={styles.input} onChangeText={(clientPhoneNumber) => this.setState({ clientPhoneNumber })}
                  value={this.state.clientPhoneNumber}
                  placeholder="شماره تلفن" />
                <Button title="ثبت" onPress={
                  async () => {
                    await Storage.setClientPhoneNumber(this.state.clientPhoneNumber);
                    Toast.show({
                      text: 'شماره تلفن ثبت شد',
                      duration: 2000
                    });
                  }}></Button>
              </Col>
            ) : null
          }

        </Content>
      </Container >
    );
  }

}