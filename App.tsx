import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import ButtonsScreen from './Screens/ButtonsScreen';
import HistoryScreen from './Screens/HistoryScreen';
import { AntDesign } from '@expo/vector-icons';
import SettingScreen from './Screens/Setting';
import { Root } from 'native-base';

const navigator = createMaterialBottomTabNavigator(
  {
    Buttons: {
      screen: ButtonsScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <AntDesign name="appstore1" size={23} color={tintColor} />
        )
      }
    },
    History: {
      screen: HistoryScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <AntDesign name="clockcircle" size={23} color={tintColor} />
        )
      }
    },
    Setting: {
      screen: SettingScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <AntDesign name="setting" size={23} color={tintColor} />
        )
      }
    }
  },
  {
    initialRouteName: 'Buttons',
    labeled: false
  }
);

const AppNavigator = createAppContainer(navigator);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;