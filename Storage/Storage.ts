import moment from "moment-jalaali";
import { AsyncStorage } from "react-native";
import { IHistoryItem } from "./IHistoryItem";

export class Storage {

    static readClientPhoneNumber(): Promise<string | null> {
        return AsyncStorage.getItem("ClientPhoneNumber");
    }

    static setClientPhoneNumber(value: string): Promise<void> {
        return AsyncStorage.setItem("ClientPhoneNumber", value);
    }

    static async readHistory(): Promise<IHistoryItem[]> {
        return JSON.parse(await AsyncStorage.getItem("History") || "[]");
    }

    static async clearHistory(): Promise<void> {
        await AsyncStorage.setItem("History","[]");
    }

    static async addToHistory(label: string): Promise<void> {
        let history = await this.readHistory();

        history.unshift({
            timestamp: moment().format('jYYYY/jM/jD HH:mm:ss'),
            label: label
        });

        if (history.length > 30) {
            history = history.slice(0, 29);
        }

        await AsyncStorage.setItem("History", JSON.stringify(history));
    }
}