export interface IHistoryItem {
    timestamp: string;
    label: string;
}